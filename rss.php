<?php
/**
 * rss
 * Lector de sitios rss
 * @author Daniel Huidobro <daniel.hui287@gmail.com>
 */
class rss {
    
    
    public $items;
    
    /**
     * rss
     * @param type $url
     */
    
    function __construct($url) {
        try {
            $content = file_get_contents($url);
            $rssContent = new SimpleXmlElement($content);
            $this->items = $rssContent->channel->item;
        } catch (Exception $ex) {
            die('Error al leer: ' . $url . PHP_EOL . ' ' . $ex);
        }
    }

}