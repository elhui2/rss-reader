<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once 'rss.php';
$rss = new rss('http://estaticos.marca.com/rss/futbol/primera-division.xml');
?>

<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Rss Reader</title>
        <link href='css/rss.css' rel='stylesheet' type='text/css'>
    </head>
    <body>

        <nav class="top-bar" data-topbar>
            <h1 class="title"><a href="/">Rss Reader</a></h1>
        </nav>
        
        <?php
        $x=0;
        foreach ($rss->items as $key => $item) {
            ?>
        
            <article class="rss-item">
                
                <h4><a target="_blank" href="<?php echo $item->link ?>"><?php echo $item->title ?></a></h4>
                
                <h6><?php echo $item->children('media', true)->title; ?></h6>
                
                <div class="desc">
                    <img src="<?php echo $item->children('media', true)->thumbnail->attributes()->url; ?>" alt="<?php echo $item->title ?>" class="thumnbnail">
                    <?php $description = strip_tags($item->children('media', true)->description); ?>
                    <p><?php echo substr( $description , 0 , 120  ); ?></p>
                </div>
                <div class="tags">
                    <?php foreach ($item->category as $category) { ?>
                        <span class="tag"><?php echo $category ?></span>
                        <?php
                    }
                    ?>
                </div>

            </article>

            <?php
            if($x>=19){
                break;
            }else{
                $x++;
            }
        }
        ?>

        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="js/app.js"></script>
    </body>
</html>